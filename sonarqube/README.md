
# Create a database.

The way I have setup sonarqube is to use a local postgres server running in k8s.

The configmap is not setup for sonarqube; it only sets up the admin user.
To create a sonarqube user and database follow these steps:

1. Get a pod name for postgres: `microk8s.kubectl get pods`
2. Get a shell into the pod: `microk8s.kubectl exec -it postgres-deployment-5585878c54-g7knd -- bash`
3. change to postgres user: `su - postgres`
4. Get psql shell: `psql -d sandbox -U admin`
5. Create sonar user: `create role sonar with login password 'sonar';`
6. Create sonarqube db: `create database sonar with owner sonar;`


# Common Errors:

`max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]`

Solve this by: `sysctl -w vm.max_map_count=262144`

To set this value permanently, update the vm.max_map_count setting in `/etc/sysctl.conf`. To verify after rebooting, run `sysctl vm.max_map_count`.

I have also noticed in `/etc/sysctl.conf` the following option:

```
Uncomment the next line to enable packet forwarding for IPv4
net.ipv4.ip_forward=1
```

# Get the logs

```
microk8s.kubectl logs -f deployment/sonarqube-deployment
```
