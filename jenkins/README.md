## Deploy using Microk8s

 * microk8s.kubectl apply -f deployment.yml
 * microk8s.kubectl apply -f service.yml
 * microk8s.kubectl apply -f ingres.yml
 
 
## Volumes using NFS !! INSECURE !!

* Please note I have not taken steps to harden this setup yet!!

I have tried HostPath in k8s for volume mounts but it is not very reliable.
This time I will use NFS, running on my workstation.

Install using: `sudo apt install nfs-kernel-server`

Create the directory where we will host our data: `sudo mkdir -p /data/nfs/jenkins`

Alter directory permissions: `sudo chown nobody:nogroup /data/nfs/jenkins`

Edit `/etc/exports` and add `/data/nfs/jenkins    *(rw,sync,insecure,no_subtree_check,sync)`

Restart the service: `sudo systemctl restart nfs-kernel-server`

Allow nfs access: $ `sudo ufw allow nfs`


## Unlock jenkins 
 
If everything went well and jenkins is running on `jenkins.localhost', it will likely ask for the admin password.

1. Get a list of the running pods: `microk8s.kubectl get pods`
2. Get a shell in (one of) the pod(s): `microk8s.kubectl exec -it << PODNAME >> -- bash`
3. Alternatively if everything has gone right you can take a look in the NFS directory for jenkins.

