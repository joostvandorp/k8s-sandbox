initialize the DB
```
odoo -d odoo -i base --no-http --stop-after-init
```


shell into odoo pod

```
$ microk8s.kubectl -n odoo get pods
$ microk8s.kubectl -n odoo exec --stdin --tty odoo-deployment-59ccd97768-x7cpx -- /bin/bash
```
