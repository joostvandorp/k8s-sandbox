# Reset microk8s

If you've done something, and want to return to a "clean" state run `microk8s.reset`
It takes a little while because it finds and destroys configs, pvcs, pods and cleans up all your installed addons.

Once reset, you'll need to re-install your addons, delete your config file and recopy it.


# Deploy using Microk8s

The easiest way to run these containers is to use Ubuntu (18.04). Microk8s is packaged as a snap and is available on other variations of Linux as well.

$ `sudo snap install microk8s --classic`

Add your user to the microk8s group so we don't have to use sudo anymore.
$ `sudo usermod -G microk8s -a $USER`

Configure UFW to allow traffic between pods.
$ `sudo ufw enable`
$ `sudo ufw allow in on cni0 && sudo ufw allow out on cni0`
$ `sudo ufw default allow routed`
$ `sudo ufw status`

Enable DNS and Ingres

$ `microk8s.enable dns ingress`


## Deploying the dashboard

$ `microk8s.enable dashboard`

If RBAC is not enabled access the dashboard using the default token retrieved with:

token=$(microk8s.kubectl -n kube-system get secret | grep default-token | cut -d " " -f1)
microk8s.kubectl -n kube-system describe secret $token

In an RBAC enabled setup (microk8s.enable RBAC) you need to create a user with restricted
permissions as shown in:
https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md


## Extra Notes for future me

This time around I deployed on ubuntu server.
I enabled metallb which worked almost out of the box.
No need to setup routes in the router anymore.

I did need to modify dhcp in the router and restrict the IP's we are handing out. 
netmask in the router is 255.255.255.0
dhcp range in router is 192.168.1.0/25 (min 1, max 126, broadcast 127)

I set metalLB to be allows to use 200-250.
What I should have done, is allow:

192.168.192/26 (min 193, max, 254, broadcast 254)

Use an IP calculator next time.

Anyways, as long as dhcp (controlled via router) is on the same subnet as metallb (without contflicting IP range)
everything should play nice.

Since I used ubuntu server, I didn't need to modify the firewall.



## Howto Read Logs
Without needing to know the exact pod name/id we can follow all the logs of a deployment.

microk8s.kubectl logs -f deployment/redis-deployment


## How install access a "remote" microk8s service

On your work machine, install `kubectl`
Configure kubectl by creating ~/.kube/config

    Where does config come from?

$ `microk8s config > config`
$ `scp user@server:~/path/to/config ~/.kube/config`


## How to deal with diskspace
On a single-node that uses microk8s we can use openebs

$ `microk8s.enable openebs`

I am quite sure everything works out of the box when using:

   storageClassName: openebs-hostpath

I am not sure where on the host-device files are stored, but in my yaml files, I don't need to specify the host-directory.
I just need to create a PVC and specify it in my deployement.yaml files.

continue using instructions from here: https://kubernetes.io/docs/tasks/administer-cluster/change-default-storage-class/#changing-the-default-storageclass

To Check the available storageClasses available on k8s, you can run the following command:

$ `kubectl get storageclass`

    NAME                       PROVISIONER           RECLAIMPOLICY   VOLUMEBINDINGMODE      ALLOWVOLUMEEXPANSION   AGE
    openebs-jiva-csi-default   jiva.csi.openebs.io   Delete          Immediate              true                   3d2h
    openebs-device             openebs.io/local      Delete          WaitForFirstConsumer   false                  3d2h
    openebs-hostpath           openebs.io/local      Delete          WaitForFirstConsumer   false                  3d2h


$ `kubectl patch storageclass openebs-hostpath -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'`


