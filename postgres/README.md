
## Special Notice

It seems to be importabt for posgres to have `no_root_squash` as part of its NFS permissions on the server.


`/data/nfs/postgres *(rw,sync,insecure,no_subtree_check,sync,no_root_squash)`

